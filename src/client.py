import numpy as np
import csv
import os
from id3 import learn, predict

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn import preprocessing

def main():

    X, y = get_mushroom_data()

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=42)

    im = 'entropy'

    #Train and predict with my implementation.
    tree = learn(X_train, y_train, impurity_measure=im)
    tree.show()

    errors = 0
    for i in range(len(X_test)):
        if predict(X_test[i], tree) != y_test[i]:
            errors += 1

    print("errors", errors)

    #And now with sklearn implementation.
    X_train, X_test, y_train, y_test = encode(X_train, X_test, y_train, y_test)
    clf = DecisionTreeClassifier(criterion=im)
    clf.fit(X_train, y_train)

    errors_sklearn = 0
    for i in range(len(X_test)):
        if clf.predict([X_test[i]]) != y_test[i]:
            errors_sklearn += 1  

    print("errors_sklearn", errors_sklearn)

def get_mushroom_data():
    filename = 'agaricus-lepiota.data'
    filepath = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'data', filename))

    data = []
    with open(filepath) as file:
        reader = csv.reader(file)
        for row in reader:
            if not '?' in row:
                data.append(row)

    data = np.array(data)
    X = data[:, 1:]
    y = data[:, 0]
    return X, y 

def encode(X_train, X_test, y_train, y_test):
    le = preprocessing.LabelEncoder()
    for i in range(len(X_train)):
        X_train[i] = le.fit_transform(X_train[i])

    for i in range(len(X_test)):
        X_test[i] = le.fit_transform(X_test[i])

    y_train = le.fit_transform(y_train)
    y_test = le.fit_transform(y_test)
    return X_train, X_test, y_train, y_test

if __name__ == '__main__':
    main()

