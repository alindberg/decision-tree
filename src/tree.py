class Tree:
    """
    Custom general tree for a decision tree.
    """
    def __init__(self, branch=None, classifier=None):
        """
        Args:
            branch (str): The name of the branch that lead to this node.
            classifier (str): Set if this is a leaf node.
        """
        self.children = []
        self.branch = branch
        self.classifier = classifier

    def append_child(self, child):
        self.children.append(child)

    def remove_child(self, child):
        self.children.remove(child)

    def clear_children(self):
        self.children = []
        
    def is_leaf(self):
        """
        Returns:
            Whether this Tree is one without any child nodes.
        """
        return self.children == []

    def __repr__(self):
        return str(self.branch)

    def show(self):
        """
        "pretty" print this tree.
        """
        if self.is_leaf():
            print(self.branch, " -> ", self.classifier, " (Leaf)")
        else:
            print(self.branch, "-> ", self.children)
            
            for child in self.children:
                child.show()